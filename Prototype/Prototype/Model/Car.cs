﻿using Prototype.Abstractions;

namespace Prototype.Model
{
    public class Car: Transport, IMyCloneable<Car>, ICloneable
    {
        /// <summary>
        /// Признак явялется ли кабриолетом 
        /// </summary>
        public bool IsCabriolet { get; set; }

        public Car() { }

        public Car(Car car) : base(car.EngineVolume, car.EnginePower, car.Name, car.KindOfTransport)
        {
            IsCabriolet = car.IsCabriolet;
        }

        public Car(int engineVolume, int enginePower, string name, string kindOfTransport, bool isCabriolet) : base(engineVolume, enginePower, name, kindOfTransport)
        {
            KindOfTransport = kindOfTransport;
            IsCabriolet = isCabriolet;
        }

        public override string Move() => "Машина отправилась в путешествие";

        public override Car MyClone()
        {
            return new Car(this);
        }

        public override string ToString() =>
            $"EngineVolume: {EngineVolume} | EnginePower: {EnginePower} | Name: {Name} | KindOfTransport: {KindOfTransport}, IsCabriolet: {IsCabriolet}, {Move()}";

    }
}
