﻿using Prototype.Abstractions;

namespace Prototype.Model
{
    public class HondaQuad: Motorcycle, IMyCloneable<HondaQuad>
    {
        /// <summary>
        /// Признак количества 4 колес
        /// </summary>
        public bool IsFourWheels { get; set; }

        public HondaQuad() { }

        public HondaQuad(HondaQuad quad) : base(quad.EngineVolume, quad.EnginePower, quad.Name, quad.KindOfTransport, quad.IsTwoWheels)
        {
            IsFourWheels = quad.IsFourWheels;
        }

        public HondaQuad(int engineVolume, int enginePower, string name, string kindOfTransport, bool isTwoWheels, bool isFourWheels)
            : base(engineVolume, enginePower, name, kindOfTransport, isTwoWheels)
        {
            IsFourWheels = isFourWheels;
        }

        public override string Move() => "Квадроцилк отправился в путь";

        public override HondaQuad MyClone()
        {
            return new HondaQuad(this);
        }

        public override string ToString() =>
            $"EngineVolume: {EngineVolume} | EnginePower: {EnginePower} | Name: {Name} | KindOfTransport: {KindOfTransport} | IsFourWheels: {IsFourWheels} | IsTwoWheels: {IsTwoWheels}, {Move()}";

    }
}
