﻿using Prototype.Abstractions; 

namespace Prototype.Model
{
    public class Motorcycle : Transport, IMyCloneable<Motorcycle>, ICloneable
    {
        // <summary>
        /// Признак количества двух колес
        /// </summary>
        public bool IsTwoWheels { get; set; }

        public Motorcycle() { }

        public Motorcycle(Motorcycle moto) : base(moto.EngineVolume, moto.EnginePower, moto.Name, moto.KindOfTransport)
        {
            IsTwoWheels = moto.IsTwoWheels;
        }

        public Motorcycle(int engineVolume, int enginePower, string name, string kindOfTransport, bool twoWheels) : base(engineVolume, enginePower, name, kindOfTransport)
        {
            KindOfTransport = kindOfTransport;
            IsTwoWheels = IsTwoWheels;
        }

        public override string Move() => "Мотоцикл отправился в путь";

        public override Motorcycle MyClone()
        {
            return new Motorcycle(this);
        }

        public override string ToString() =>
            $"EngineVolume: {EngineVolume} | EnginePower: {EnginePower} | Name: {Name} | KindOfTransport: {KindOfTransport}, TwoWheels: {IsTwoWheels}, {Move()}";

    }
}
